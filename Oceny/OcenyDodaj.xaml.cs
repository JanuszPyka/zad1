﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
   
    public partial class OcenyDodaj : Page
    {
        public OcenyDodaj()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            string wynik = dodaj_ocena(Int32.Parse(ocena.Text), ocenas.Text, data.Text, przedmiot.Text, prowadzacy.Text, Int32.Parse(ids.Text));
            
            MessageBox.Show(wynik);
        }

        public string dodaj_ocena(int ocena, string Ocenaslownie, string datawystawieniaOceny, string nazwaprzedmiotu, string nazwiskoprowadzacego, int IdStudnet)
        {
            string cs = @"URI=file:D:/baza.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);


            cmd.CommandText = "INSERT INTO oceny (ocena, Ocenaslownie, datawystawieniaOceny, nazwaprzedmiotu, nazwiskoprowadzacego, IdStudnet)" +
                "VALUES(@ocena, @Ocenaslownie, @datawystawieniaOceny, @nazwaprzedmiotu, @nazwiskoprowadzacego, @IdStudnet)";

            cmd.Parameters.AddWithValue("@ocena", ocena);
            cmd.Parameters.AddWithValue("@Ocenaslownie", Ocenaslownie);
            cmd.Parameters.AddWithValue("@datawystawieniaOceny", datawystawieniaOceny);
            cmd.Parameters.AddWithValue("@nazwaprzedmiotu", nazwaprzedmiotu);
            cmd.Parameters.AddWithValue("@nazwiskoprowadzacego", nazwiskoprowadzacego);
            cmd.Parameters.AddWithValue("@IdStudnet", IdStudnet);
            cmd.Prepare();

            cmd.ExecuteNonQuery();

            return ("Dodano ocenę");
        }
    }
}
