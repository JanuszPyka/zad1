﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
  
    public partial class OcenyPodglad : Page
    {
        public List<oceny> lista;
        public OcenyPodglad()
        {
            InitializeComponent();
            lista = wyswielt();
            wyswietl.DataContext = lista;
        }

        public List<oceny> wyswielt()
        {
            string baza = @"URI=file:D:/baza.db";
            var connect = new SQLiteConnection(baza);
            connect.Open();
            string ask = "SELECT * FROM oceny";
            var zapytanie = new SQLiteCommand(ask, connect);
            SQLiteDataReader reader = zapytanie.ExecuteReader();
            var lista = new List<oceny> { };
            while (reader.Read())
            {
                lista.Add(
                    new oceny()
                    {
                        id = reader.GetInt32(0),
                        ocena = reader.GetInt32(1),
                        Ocenaslownie = reader.GetString(2),
                        datawystawieniaOceny = reader.GetString(3),
                        nazwaprzedmiotu = reader.GetString(4),
                        nazwiskoprowadzacego = reader.GetString(5),
                        IdStudnet = reader.GetInt32(6)
                    });
            }
            return lista;
        }
    }

    
}
