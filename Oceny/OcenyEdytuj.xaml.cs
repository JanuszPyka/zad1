﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
  
    public partial class OcenaEdycja : Page
    {
        public OcenaEdycja()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            string wynik = edytuj_ocena(Int32.Parse(ocena.Text), ocenas.Text, data.Text, przedmiot.Text, prowadzacy.Text, Int32.Parse(ids.Text), Int32.Parse(ido.Text));

            MessageBox.Show(wynik);
        }

        public string edytuj_ocena(int ocena, string Ocenaslownie, string datawystawieniaOceny, string nazwaprzedmiotu, string nazwiskoprowadzacego, int IdStudnet, int id)
        {
            string cs = @"URI=file:D:/baza.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);


            cmd.CommandText = "UPDATE oceny SET ocena=@ocena, Ocenaslownie=@Ocenaslownie, datawystawieniaOceny=@datawystawieniaOceny, nazwaprzedmiotu=@nazwaprzedmiotu, nazwiskoprowadzacego=@nazwiskoprowadzacego, IdStudnet=@IdStudnet" +
                " WHERE id=@id";

            cmd.Parameters.AddWithValue("@ocena", ocena);
            cmd.Parameters.AddWithValue("@Ocenaslownie", Ocenaslownie);
            cmd.Parameters.AddWithValue("@datawystawieniaOceny", datawystawieniaOceny);
            cmd.Parameters.AddWithValue("@nazwaprzedmiotu", nazwaprzedmiotu);
            cmd.Parameters.AddWithValue("@nazwiskoprowadzacego", nazwiskoprowadzacego);
            cmd.Parameters.AddWithValue("@IdStudnet", IdStudnet);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Prepare();

            cmd.ExecuteNonQuery();

            return ("Edytowano ocenę");
        }
    }
}
