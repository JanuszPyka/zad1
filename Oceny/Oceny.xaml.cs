﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dziekanat
{
    
    public partial class Oceny : Page
    {
        public Oceny()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            oceny.Content = new OcenyPodglad();
        }
        private void BtnClick2(object sender, RoutedEventArgs e)
        {
            oceny.Content = new OcenyDodaj();
        }
        private void BtnClick3(object sender, RoutedEventArgs e)
        {
            oceny.Content = new OcenyUsun();
        }
        private void BtnClick4(object sender, RoutedEventArgs e)
        {
            oceny.Content = new OcenaEdycja();
        }
    }

    public class oceny
    {
        public int id { get; set; }
        public int ocena { get; set; }
        public string Ocenaslownie { get; set; }
        public string datawystawieniaOceny { get; set; }
        public string nazwaprzedmiotu { get; set; }
        public string nazwiskoprowadzacego { get; set; }
        public int IdStudnet { get; set; }
    }
}
