﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dziekanat
{
  
    public partial class Studenci : Page
    {
        public Studenci()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            studenci.Content = new StudenciPodglad();
        }
        private void BtnClick2(object sender, RoutedEventArgs e)
        {
            studenci.Content = new StudenciDodaj();
        }
        private void BtnClick3(object sender, RoutedEventArgs e)
        {
            studenci.Content = new StudenciUsun();
        }
        private void BtnClick4(object sender, RoutedEventArgs e)
        {
            studenci.Content = new StudenciEdytuj();
        }
    }

    public class studenci
    {
        public int nralbumu { get; set; }
        public int pesel { get; set; }
        public string imie { get; set; }
        public string nazwisko { get; set; }
        public string dataurodzenia { get; set; }
        public string miejscowosc { get; set; }
        public string kierunekstudiow { get; set; }
    }
}
