﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
    
    public partial class StudenciDodaj : Page
    {
        public StudenciDodaj()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            string wynik = dodaj_student(Int32.Parse(pesel.Text), imie.Text, nazwisko.Text, datau.Text, miejscowosc.Text, kieruneks.Text);

            MessageBox.Show(wynik);
        }

        public string dodaj_student(int pesel, string imie, string nazwisko, string dataurodzenia, string miejscowosc, string kierunek)
        {
            string cs = @"URI=file:D:/baza.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);


            cmd.CommandText = "INSERT INTO studenci (pesel, imie, nazwisko, dataurodzenia, miejscowosc, kierunekstudiow)" +
                "VALUES(@pesel, @imie, @nazwisko, @dataurodzenia, @miejscowosc, @kierunekstudiow)";

            
            cmd.Parameters.AddWithValue("@pesel", pesel);
            cmd.Parameters.AddWithValue("@imie", imie);
            cmd.Parameters.AddWithValue("@nazwisko", nazwisko);
            cmd.Parameters.AddWithValue("@dataurodzenia", dataurodzenia);
            cmd.Parameters.AddWithValue("@miejscowosc", miejscowosc);
            cmd.Parameters.AddWithValue("@kierunekstudiow", kierunek);
            cmd.Prepare();

            cmd.ExecuteNonQuery();

            return ("Dodano studenta");
        }
    }
}
