﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
    
    public partial class StudenciPodglad : Page
    {
        public List<studenci> lista;
        public StudenciPodglad()
        {
            InitializeComponent();
            lista = wyswielt();
            wyswietl.DataContext = lista;
        }

        public List<studenci> wyswielt()
        {
            string baza = @"URI=file:D:/baza.db";
            var connect = new SQLiteConnection(baza);
            connect.Open();
            string ask = "SELECT * FROM studenci";
            var zapytanie = new SQLiteCommand(ask, connect);
            SQLiteDataReader reader = zapytanie.ExecuteReader();
            var lista = new List<studenci> { };
            while (reader.Read())
            {
                lista.Add(
                    new studenci()
                    {
                        nralbumu = reader.GetInt32(0),
                        pesel = reader.GetInt32(1),
                        imie = reader.GetString(2),
                        nazwisko = reader.GetString(3),
                        dataurodzenia = reader.GetString(4),
                        miejscowosc = reader.GetString(5),
                        kierunekstudiow = reader.GetString(6)
                    });
            }
            return lista;
        }
    }

    
}
