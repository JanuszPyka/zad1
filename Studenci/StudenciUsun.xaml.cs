﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
   
    public partial class StudenciUsun : Page
    {
        public StudenciUsun()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            string wynik = usun_student(Int32.Parse(nralbumu.Text));

            MessageBox.Show(wynik);
        }

        public string usun_student(int id_remove)
        {
            string cs = @"URI=file:D:/baza.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);
            
            cmd.CommandText = "DELETE FROM studenci WHERE nralbumu=@id_remove";

            cmd.Parameters.AddWithValue("@id_remove", id_remove);
            cmd.Prepare();

            cmd.ExecuteNonQuery();
            return ("Usunięto studenta");
        }
    }
}
