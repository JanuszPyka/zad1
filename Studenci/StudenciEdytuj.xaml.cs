﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SQLite;

namespace Dziekanat
{
    
    public partial class StudenciEdytuj : Page
    {
        public StudenciEdytuj()
        {
            InitializeComponent();
        }

        private void BtnClick1(object sender, RoutedEventArgs e)
        {
            string wynik = edytuj_student(Int32.Parse(pesel.Text), imie.Text, nazwisko.Text, datau.Text, miejscowosc.Text, kieruneks.Text, Int32.Parse(nralbumu.Text));

            MessageBox.Show(wynik);
        }

        public string edytuj_student(int pesel, string imie, string nazwisko, string dataurodzenia, string miejscowosc, string kierunek, int nralbumu)
        {
            string cs = @"URI=file:D:/baza.db";

            var con = new SQLiteConnection(cs);
            con.Open();

            var cmd = new SQLiteCommand(con);


            cmd.CommandText = "UPDATE studenci SET pesel = @pesel, imie = @imie, nazwisko = @nazwisko, dataurodzenia = @dataurodzenia, miejscowosc = @miejscowosc, kierunekstudiow = @kierunekstudiow" +
                " WHERE nralbumu = @nralbumu";


            cmd.Parameters.AddWithValue("@pesel", pesel);
            cmd.Parameters.AddWithValue("@imie", imie);
            cmd.Parameters.AddWithValue("@nazwisko", nazwisko);
            cmd.Parameters.AddWithValue("@dataurodzenia", dataurodzenia);
            cmd.Parameters.AddWithValue("@miejscowosc", miejscowosc);
            cmd.Parameters.AddWithValue("@kierunekstudiow", kierunek);
            cmd.Parameters.AddWithValue("@nralbumu", nralbumu);
            cmd.Prepare();
            //MessageBox.Show(cmd.CommandText);
            cmd.ExecuteNonQuery();

            return ("Edytowano studenta");
        }
    }
}
